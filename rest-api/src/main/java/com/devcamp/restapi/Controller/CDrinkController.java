package com.devcamp.restapi.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.function.ServerRequest.Headers;

import com.devcamp.restapi.model.CDrink;
import com.devcamp.restapi.repository.IDrinkRepository;



@CrossOrigin
@RestController
@RequestMapping("/")
public class CDrinkController {
    @Autowired
    IDrinkRepository pCustomRepository;
    
    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllCDrinks(){
        try{
            List<CDrink> listCustomer = new ArrayList<CDrink>();

            pCustomRepository.findAll()
            .forEach(listCustomer::add);
            return new ResponseEntity<>(listCustomer, HttpStatus.OK);

        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
